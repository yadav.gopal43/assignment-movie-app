import React from 'react';

import classes from './Backdrop.module.css';

const BackDrop = props => props.show ? <div className={classes.Backdrop}></div> : null


export default BackDrop;