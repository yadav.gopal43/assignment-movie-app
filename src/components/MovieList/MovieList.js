import React from 'react';

import classes from './MovieList.module.css';
import MovieCard from '../MovieCard/MovieCard';
import { useDispatch } from 'react-redux';
import { ADD_MOVIE_TO_FAVOURITES, REMOVE_MOVIE_FROM_FAVOURITES } from '../../store/actions/actionTypes';

const MovieList = props => {

    const dispatch = useDispatch();

    const addToFavourites = movie => {
        dispatch({
            type: ADD_MOVIE_TO_FAVOURITES,
            payload: movie
        });
    }

    const removeFromFavourites = id => {
        dispatch({
            type: REMOVE_MOVIE_FROM_FAVOURITES,
            payload: id
        });
    }

    return (
        <React.Fragment>
            <div className={`card-container d-flex flex-row flex-nowrap ${classes.MovieList}`}>
                {
                    props.movies.map((movie) => {
                        return <MovieCard
                            key={movie.id}
                            title={movie.title}
                            imagePath={movie.poster_path}
                            clicked={props.canAddToFavourite ? () => addToFavourites(movie) : undefined}
                            deleteHandler={props.showDelete ? () => removeFromFavourites(movie.id) : undefined}/>
                    })
                }
            </div>
        </React.Fragment>
    )
}

export default MovieList;