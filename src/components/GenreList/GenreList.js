import React, { useEffect, useState } from 'react';
import { getMoviesByGenre } from '../../apis/moviesApiCalls';
import MovieList from '../MovieList/MovieList';
import Spinner from '../UI/Spinner/Spinner';
import { ADD_MOVIE_IN_FEATURED_MOVIES } from '../../store/actions/actionTypes';
import { useDispatch } from 'react-redux';

const GenreList = props => {

    const dispatch = useDispatch();

    let movieListView = null;
    const [movies, setMovies] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [isError, setError] = useState(false);

    useEffect(() => {
        if (props.id) {
            getMoviesByGenre(props.id)
                .then((response) => {
                    setLoading(false);
                    setError(false);
                    setMovies(response);

                    dispatch({
                        type: ADD_MOVIE_IN_FEATURED_MOVIES,
                        payload: response[0]
                    });
                })
                .catch((error) => {
                    setError(true);
                })
        }
    }, [props.id, dispatch])

    if (isLoading) {
        movieListView = <Spinner />
    } else {
        movieListView = <MovieList
            movies={movies}
            canAddToFavourite="true" />
    }

    if (isError) {
        movieListView = <p className="text-info mb-5 lead">
            "Oops!! Not able to fetch movies for genre { props.name }"
        </p>
    }

    return (
        <React.Fragment>
            <h3>{props.name}</h3>
            {movieListView}
        </React.Fragment>
    )
}

export default GenreList;