import React from 'react';
import classes from './MovieCard.module.css';
import { MOVIE_POSTER_PATH } from '../../apis/EndPoint.constant';

const MovieCard = props => {

    let deleteButton = null;

    if (props.deleteHandler) {
        deleteButton = (
            <div className={classes.Overlay}>
                <button className={`btn ${classes.DeleteBtn}`}
                    onClick={props.deleteHandler}>
                    <i className="fa fa-trash fa-3x"></i>
                </button>
            </div>
        )
    }

    return (
        <div
            className={`card card-body ${classes.MovieCard}`}
            onClick={props.clicked} >

            <img src={`${MOVIE_POSTER_PATH}${props.imagePath}`}
                alt={props.title}>
            </img>

            {deleteButton}
        </div>
    )
}

export default MovieCard;