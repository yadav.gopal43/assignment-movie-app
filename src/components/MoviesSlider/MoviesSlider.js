import React from 'react';
import Carousel from 'react-bootstrap/Carousel';

import { MOVIE_BANNER_PATH } from '../../apis/EndPoint.constant';

const MoviesSlider = props => {
    return (
        <div>
            <Carousel>
                {
                    props.movies.map(movie => {
                        return (
                            <Carousel.Item style={{ 'height': "250px" }} key={movie.id} >
                                <img style={{ 'height': "250px" }}
                                    className="d-block w-100"
                                    src={`${MOVIE_BANNER_PATH}${movie.backdrop_path}`}
                                    alt={movie.title} />
                                <Carousel.Caption>
                                    <h3>{movie.title}</h3>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )
                    })
                }
            </Carousel>
        </div>
    );
}

export default MoviesSlider;