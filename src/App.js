import React from 'react';
import './App.css';
import FavouriteMovies from './containers/FavouriteMovies/FavouriteMovies';
import MoviesByGenre from './containers/MoviesByGenre/MoviesByGenre';
import FeaturedMoviesSlider from './containers/FeaturedMoviesSlider/FeaturedMoviesSlider';

function App() {
  return (
    <div>
      <FeaturedMoviesSlider />
      <div className="container-fluid">
        <FavouriteMovies />
        <MoviesByGenre />
      </div>
    </div>
  );
}

export default App;
