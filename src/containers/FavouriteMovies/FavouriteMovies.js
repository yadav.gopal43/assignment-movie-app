import React from 'react';
import { useSelector } from 'react-redux';

import MovieList from '../../components/MovieList/MovieList'

const FavouriteMovies = () => {

    const { favouriteMovies } = useSelector(state => state.favouriteMovies);

    let favMovies = <p className="text-center lead">“Nothing here! Scroll to discover more”</p>;

    if (favouriteMovies.length > 0) {
        favMovies = <MovieList
            movies={favouriteMovies}
            showDelete="true" />
    }

    return (
        <React.Fragment>
            <h3>My List</h3>
            {favMovies}
        </React.Fragment>
    )
}

export default FavouriteMovies;