import React, { Component } from 'react';

import GenreList from '../../components/GenreList/GenreList';
import { getGenres } from '../../apis/moviesApiCalls';
import Modal from '../../components/UI/Modal/Modal';

class MoviesByGenre extends Component {

    state = {
        genres: [],
        isLoading: true,
        isError: false
    }

    componentDidMount() {
        getGenres()
            .then((genreList) => {
                this.setState({
                    genres: genreList,
                    isLoading: false,
                    isError: false
                });
            })
            .catch(error =>
                this.setState({
                    isLoading: false,
                    isError: true
                })
            );
    }

    render() {

        let genreListView = null;

        if (this.state.isLoading) {
            genreListView = <p>LOADING ...</p>
        }

        if (this.state.genres.length > 0) {
            genreListView = this.state.genres.map(genre => {
                return (
                    <GenreList
                        key={genre.id}
                        id={genre.id}
                        name={genre.name} />
                )
            })
        }

        if (this.state.isError) {
            genreListView = (<Modal show="true">
                <p>OOPS!! Something went wrong. Please try again after some time.</p>
            </Modal>)
        }

        return (
            <div>
                {genreListView}
            </div>
        )
    }
}

export default MoviesByGenre;