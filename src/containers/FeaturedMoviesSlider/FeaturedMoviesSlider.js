import React from 'react';
import { useSelector } from 'react-redux';
import MoviesSlider from '../../components/MoviesSlider/MoviesSlider';
import classes from './FeaturedMoviesSlider.module.css'

const FeaturedMoviesSlider = () => {
    const { featuredMovies } = useSelector(state => state.featuredMovies);

    return (
        <div className={`mb-5 ${classes.SliderContainer}`}>
            {featuredMovies.length > 0 ? <MoviesSlider movies={featuredMovies} /> : null}
        </div >
    );
}

export default FeaturedMoviesSlider;