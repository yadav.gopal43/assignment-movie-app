import axios from 'axios';

// It should be stored and fetched from process.env variables
const API_KEY =  process.env.REACT_APP_API_KEY;

const instance = axios.create({
    baseURL: 'https://api.themoviedb.org/3/'
});

instance.interceptors.request.use((config) => {

    config.params = config.params || {};
    config.params['api_key'] = API_KEY;
    return config;
});

export default instance;