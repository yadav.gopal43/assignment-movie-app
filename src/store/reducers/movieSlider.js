/* This is a bit messed up. I wanted to have images path to show on the
    slider. Ideally, I should have resolved API calls to get movies by GenreID
    using a reducer and stored their state and used it in Movie List component.
    OR could have used Context API to collect the movies by genre and
    pass it downstream to Genre > movies component 
    I misunderstood the API's, thought there would be some API to get promotional
    or featured movies and I could use that to show images on slider.

    Do not want to waste more time on it so using API calls made in GenreList
    to pick some movies and making them available for slider.
*/

import { ADD_MOVIE_IN_FEATURED_MOVIES } from "../actions/actionTypes";

const initialState = {
    // Picking the first movie from each genre and adding them in featured movies 
    featuredMovies: []
};

const moviesSliderReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_MOVIE_IN_FEATURED_MOVIES:
            if (state.featuredMovies.length < 6) {
                const updatedList = state.featuredMovies.concat(action.payload);
                return {
                    ...state,
                    featuredMovies: updatedList
                }
            }
            return state;

        default:
            return state;
    }
}

export default moviesSliderReducer;