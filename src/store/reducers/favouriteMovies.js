import { ADD_MOVIE_TO_FAVOURITES, REMOVE_MOVIE_FROM_FAVOURITES } from '../actions/actionTypes';

// These can be exported from a constants.js file.
// But because of limited number of application constant putting it here.
const LOCAL_STORAGE_FAVOURITE_MOVIES_TOKEN = 'FAVOURITE_MOVIES';

const initialState = {
    favouriteMovies: localStorage.getItem(LOCAL_STORAGE_FAVOURITE_MOVIES_TOKEN) ?
        JSON.parse(localStorage.getItem(LOCAL_STORAGE_FAVOURITE_MOVIES_TOKEN)) : []
};

const favouriteMoviesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MOVIE_TO_FAVOURITES:
            const movieIndex = state.favouriteMovies.findIndex(m => m.id === action.payload.id);
            if (movieIndex === -1) {
                const updatedList = state.favouriteMovies.concat(action.payload);
                localStorage.setItem(LOCAL_STORAGE_FAVOURITE_MOVIES_TOKEN, JSON.stringify(updatedList));
                return {
                    ...state,
                    favouriteMovies: updatedList
                }
            }
            return state;

        case REMOVE_MOVIE_FROM_FAVOURITES:
            // payload would have movie id
            const index = state.favouriteMovies.findIndex(m => m.id === action.payload);
            if (index > -1) {
                const updatedList = [...state.favouriteMovies];
                updatedList.splice(index, 1);
                localStorage.setItem(LOCAL_STORAGE_FAVOURITE_MOVIES_TOKEN, JSON.stringify(updatedList));
                return {
                    ...state,
                    favouriteMovies: updatedList
                }
            } else {
                // only possible if local storage values are manipulated
                // Show some error
                break;
            }

        default:
            return state;
    }
}

export default favouriteMoviesReducer;