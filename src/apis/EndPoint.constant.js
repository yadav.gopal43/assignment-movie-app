export const GET_MOVIE_GENRES = 'genre/movie/list?language=en-US';
export const GET_MOVIES_BY_GENRE = 'discover/movie';

export const MOVIE_POSTER_PATH = 'https://image.tmdb.org/t/p/w200/';
export const MOVIE_BANNER_PATH = 'https://image.tmdb.org/t/p/w500/';