import axios from '../axios-instance';

import { GET_MOVIE_GENRES, GET_MOVIES_BY_GENRE } from './EndPoint.constant';

export async function getGenres() {
    try {
        const response = await axios.get(GET_MOVIE_GENRES);
        return response.data.genres;
    }
    catch (err) {
        console.log("error", err);
        throw err;
    }
}

export async function getMoviesByGenre(genreId) {
    try {
        const response = await axios.get(GET_MOVIES_BY_GENRE, {
            params: {
                language: 'en-US',
                sort_by: 'popularity.desc',
                include_adult: false,
                page: 1,
                with_genres: genreId
            }
        });
        return response.data.results;
    }
    catch (err) {
        console.log("error", err);
        throw err;
    }
}