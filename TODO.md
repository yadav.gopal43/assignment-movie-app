### TODO

Below are some of the ToDo's which are essential considering best practices and standards but not been done yet.

 1. Add prop-type for input params checking
 2. Better placement of components (currently GenreList and MoviesByGenre seems to be shuffled)
 3. Eslint configuration and checking
 4. Unit tests - Ideally it should be added during development of each functionality.
 5. Performance - Some of the things can be optimised - Images rendering when they are available to be shown in display and some prefetch techniques can also be used.
 6. UI/UX - The UX can be improved, had different thoughts and idea. But tried to achieve functionality before diving deep into UX.
