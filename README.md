
## Movie Shortlist App (Assignment)

Api used - themoviedb https://developers.themoviedb.org/3/

### TODO - [TODO list](TODO.md)

## Available Scripts


In the project directory, you can run:

  
### Environment variable configuration
Create a file .env / .env.development / .env.production (based on the configuration you want to run) in the root folder and add the below entry in the file:

REACT_APP_API_KEY=<<actual_api_key_based_on_config>>

---
### `npm start`

 
Runs the app in the development mode.<br  />

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

  

The page will reload if you make edits.<br  />

You will also see any lint errors in the console.

  

### `npm run build`

Builds the app for production to the `build` folder.<br  />

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br  />

Your app is ready to be deployed!
